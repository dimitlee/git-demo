Just a small town girl
Living in a lonely world
She took a midnight train
Going anywhere...

Just a city boy
Born and raised in South Detroit
He took a midnight train
Going anywhere...

[INSTRUMENTAL]

A singer in a smoking room
I smell the wine and cheap perfume
For a smile they can share a night
It goes on and on and on and on...

Strangers waitin'
Up and down the boulevard
Their shadows searchin' in the night
Streetlight people
Livin' just to find emotion
Hidin' somewhere in the night

Workin' hard to get my fill
Everybody wants a thrill
Payin' anything to roll the dice just one more time
Some will win, some will lose
Some are born to sing the blues
Oh, the movie never ends
It goes on and on and on and on

Strangers waitin'
Up and down the boulevard
Their shadows searchin' in the night
Streetlight people
Livin' just to find emotion
Hidin' somewhere in the night
